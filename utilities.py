##Utility developed by Mayur

import os
from boto3.session import Session
import errno
from get_params import aws_access_key_id,aws_secret_access_key,S3_BUCKET, S3_UPLOADPATH, SOURCE_LOCATION
import shutil


def get_aws_session(new=False):

    region_name = 'us-east-2'

    session = Session(
        aws_access_key_id=aws_access_key_id,
        aws_secret_access_key=aws_secret_access_key,
        region_name=region_name
    )

    return session

def get_s3_client(new=False):
    session = get_aws_session(new)

    return session.client('s3')

def remove_directory(path):
    try:
        path = path.replace("\\", "/")
        #os.rmdir(path)
        shutil.rmtree(path)
    except OSError as e:
        print("Error: %s - %s." % (e.filename, e.strerror))

def assert_dir_exists(path):
    """
    Checks if directory tree in path exists. If not it created them.
    :param path: the path to check if it exists
    """
    try:
        os.makedirs(path)
    except OSError as e:
        if e.errno != errno.EEXIST:
            raise


def upload_file(bucket = S3_BUCKET, s3_id= aws_access_key_id, s3_pwd = aws_secret_access_key, source_local = SOURCE_LOCATION, target_s3 = S3_UPLOADPATH):
    S3_BUCKET = bucket
    aws_access_key_id = s3_id
    aws_secret_access_key = s3_pwd
    SOURCE_LOCATION = source_local
    DEST_LOCATION = target_s3

    client = get_s3_client()

    for root, dirs, files in os.walk(SOURCE_LOCATION):

        for filename in files:

            # construct the full local path
            local_path = os.path.join(root, filename)

            # construct the full Dropbox path
            relative_path = os.path.relpath(local_path, SOURCE_LOCATION)
            relative_path = relative_path.replace("\\", '/')

            s3_path = os.path.join(DEST_LOCATION, relative_path)

            # relative_path = os.path.relpath(os.path.join(root, filename))

            print('Searching "%s" in "%s"' % (s3_path, S3_BUCKET))
            try:
                client.head_object(Bucket=S3_BUCKET, Key=s3_path)
                print("Path found on S3! Skipping %s..." % s3_path)
            except:
                print("Uploading %s..." % s3_path)
                client.upload_file(local_path, S3_BUCKET, s3_path)

    #Delete the folder from local machine
    remove_directory(source_local)


def download_file(bucket = S3_BUCKET, s3_id= aws_access_key_id, s3_pwd = aws_secret_access_key, source_s3 =S3_UPLOADPATH, target_local = SOURCE_LOCATION):
    S3_BUCKET = bucket
    aws_access_key_id = s3_id
    aws_secret_access_key = s3_pwd
    SOURCE_LOCATION = source_s3
    DEST_LOCATION = target_local

    client = get_s3_client()

    # Handle missing / at end of prefix
    if not SOURCE_LOCATION.endswith('/'):
        SOURCE_LOCATION += '/'

    paginator = client.get_paginator('list_objects_v2')

    for result in paginator.paginate(Bucket=bucket, Prefix=SOURCE_LOCATION):
        print(result)
        # Download each file individually
        for key in result['Contents']:
            # Calculate relative path
            rel_path = key['Key'][len(SOURCE_LOCATION):]
            # Skip paths ending in /
            if not key['Key'].endswith('/'):
                local_file_path = os.path.join(DEST_LOCATION, rel_path)
                # Make sure directories exist
                local_file_dir = os.path.dirname(local_file_path)
                assert_dir_exists(local_file_dir)
                client.download_file(bucket, key['Key'], local_file_path)


upload_file()
#download_file()
